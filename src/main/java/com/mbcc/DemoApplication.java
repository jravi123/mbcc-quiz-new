package com.mbcc;

import com.mbcc.quiz.entity.*;
import com.mbcc.quiz.service.VertexAIAgent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
// import org.springframework.shell.standard.ShellComponent;
// import org.springframework.shell.standard.ShellMethod;

import java.util.*;

// @ShellComponent
@SpringBootApplication
public class DemoApplication {

    @Autowired
    QuesRepository quesRepository;


    @Autowired
    QuizRepository quizRepository;

    @Autowired
    UnitRepository unitRepository;
    
    @Autowired
    VertexAIAgent vertexAITest;
    
    @Autowired
    FreeResponseRepository freeResoonseRepository;
    


    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }
    

    //@ShellMethod("clean orphan questions")
    public void cleanOrphanQuestions() {
        Iterable<Ques> questions = this.quesRepository.findAll();
        for (Ques question : questions) {
            List<Quiz> quizzes = this.quizRepository.findByqIds(question.getId());
            List<String> qIds = new ArrayList<String>();
            for (Quiz quiz : quizzes) {
                qIds.add(quiz.getId());
            }

            if (qIds.size() > 0)
                question.setAttachedQuiz(qIds);
            else {
                question.setAttachedQuiz(null);
            }

            this.quesRepository.save(question);
        }
    }

    //@ShellMethod("fill units")
    public void fillUnits(){
        Iterable<Quiz> quizzes = this.quizRepository.findAll();
        for(Quiz quiz: quizzes){
            Unit unit = new Unit();
            unit.setName(quiz.getQuizname());
            Set<String> subTopics = new HashSet<String>();
            unit.setTopic(quiz.getTopic());
            Iterable<Ques> questions = this.quesRepository.findAllById(quiz.getqIds());
            for(Ques ques: questions){
                subTopics.add(ques.getSubtopic());
            }
            unit.setSubTopics(new ArrayList(subTopics));
            this.unitRepository.save(unit);
        }
    }


    //@ShellMethod("fix topic")
    public void fixTopic() {
        Iterable<Ques> questions = this.quesRepository.findAll();
        for (Ques question : questions) {
            this.quesRepository.save(question);
        }
    }


}