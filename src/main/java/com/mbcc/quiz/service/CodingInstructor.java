package com.mbcc.quiz.service;

import java.util.Scanner;

import org.springframework.ai.chat.client.ChatClient;
import org.springframework.ai.chat.client.ChatClient.ChatClientRequestSpec;
import org.springframework.ai.chat.client.advisor.MessageChatMemoryAdvisor;
import org.springframework.ai.chat.client.advisor.PromptChatMemoryAdvisor;
import org.springframework.ai.chat.client.advisor.SimpleLoggerAdvisor;
import org.springframework.ai.chat.memory.ChatMemory;
import org.springframework.ai.chat.memory.InMemoryChatMemory;
import org.springframework.ai.chat.messages.Message;
import org.springframework.ai.vertexai.gemini.VertexAiGeminiChatModel;
import org.springframework.ai.vertexai.gemini.VertexAiGeminiChatOptions;
import org.springframework.ai.chat.messages.UserMessage;

import com.google.cloud.vertexai.Transport;
import com.google.cloud.vertexai.VertexAI;

public class CodingInstructor {

	public static void main(String[] args) {

		VertexAI vertexAI = new VertexAI.Builder().setLocation(Constants.LOCATION).setProjectId(Constants.PROJECT_ID)
				.setTransport(Transport.REST).build();
		
		//code-gecko

		var geminiChatModel = new VertexAiGeminiChatModel(vertexAI,
				VertexAiGeminiChatOptions.builder().model("gemini-pro").temperature(0.2).topK(5).topP(0.95).build());
		ChatMemory chatMemory = new InMemoryChatMemory();
		String code = """
				public class Factorial {
					    public static void main(String[] args) {
					        int num = 5;
					        long factorial = 1;
					        for(int i = 1; i <= num; ++i)
					            factorial *= i;
					        System.out.println("Factorial of " + num + " = " + factorial);
					    }
					}```
				""";
		Message message = new UserMessage(code);
		chatMemory.add("Solution", message);
//            I'm Gemini, your Java instructor! I'll help you with the coding challenge step-by-step. 
//            Provide me your current approach (if any). 
//            I'll offer guidance and ask questions to nudge you towards the solution.
//            Let's work together!

		String systemMessage = """
				
			    You are a personal Java tutor helping student learn Java Programming. 

				I want you to act as my Java instructor. Don't just give me the solution. Instead, guide the student step-by-step towards understanding and solving the problem themselves.

				For example, start with a high-level explanation of the problem and the approach we should take. Then, give me one hint or question at a time to help them figure out the next part of the solution on their own.

				 Student will provide their attempts at each step, and you can give them feedback, corrections, and further guidance. You have to work together with the student to solve this!"
				Listen to the students question and guide step by step to the solution. Do you give the complete solution. Just give one hint at a time
				I repeat: DO NOT GIVE THE COMPLETE SOLUTION. LEAD THE STUDENT TO THE SOLUTION

				Problem:
				Write a Java program to find the factorial of a given number.
				
				
				""";
		



		var chatClient = ChatClient.builder(geminiChatModel)
		        .defaultSystem(systemMessage)
		        .defaultAdvisors(new PromptChatMemoryAdvisor(chatMemory))
		        .build();
		
		Scanner scanner = new Scanner(System.in);

		while (true) {
			
			String userInput = scanner.nextLine();
			
			if(userInput.equalsIgnoreCase("quit"))break;

			String response = chatClient.prompt().user(userInput).call().content();
			System.out.println("GEMINI: " + response);
		}
	}
}
