package com.mbcc.quiz.service;

import java.util.List;
import java.util.Map;

import org.springframework.ai.chat.model.ChatResponse;
import org.springframework.ai.chat.model.Generation;
import org.springframework.ai.chat.prompt.Prompt;
import org.springframework.ai.chat.prompt.PromptTemplate;
import org.springframework.ai.converter.BeanOutputConverter;
import org.springframework.ai.document.Document;
import org.springframework.ai.reader.ExtractedTextFormatter;
import org.springframework.ai.reader.pdf.PagePdfDocumentReader;
import org.springframework.ai.reader.pdf.config.PdfDocumentReaderConfig;
import org.springframework.ai.vertexai.gemini.VertexAiGeminiChatModel;
import org.springframework.ai.vertexai.gemini.VertexAiGeminiChatOptions;

import com.google.cloud.vertexai.Transport;
import com.google.cloud.vertexai.VertexAI;
import com.mbcc.quiz.entity.FreeResponse;

import reactor.core.publisher.Flux;

import org.springframework.ai.chat.messages.Message;

public class TestAP24Extraction {

	public static void main(String[] args) {

		// Read PDF documents
		// --------------------
		PagePdfDocumentReader pdfReader = new PagePdfDocumentReader("classpath:/apcsa/ap24-frq-comp-sci-a.pdf",
				PdfDocumentReaderConfig.builder().withPagesPerDocument(1).build());
		List<Document> pdfDocument = pdfReader.read();
		StringBuilder content = new StringBuilder();
	    for(Document document : pdfDocument)
	          content.append(document.getContent().trim());


		VertexAI vertexAI = new VertexAI.Builder().setLocation(Constants.LOCATION).setProjectId(Constants.PROJECT_ID)
				.setTransport(Transport.REST).build();

		var geminiChatModel = new VertexAiGeminiChatModel(vertexAI,
				VertexAiGeminiChatOptions.builder().withModel("gemini-pro").withTemperature(0.2).build());


		// convert response to a bean
		beanOutputConverter(geminiChatModel, content.toString());
	}

	public static void beanOutputConverter(VertexAiGeminiChatModel chatClient, String file) {

		BeanOutputConverter<FreeResponse> beanOutputConverter = new BeanOutputConverter<>(FreeResponse.class);


		String template = """
				Generate the Free Response object out of the pdf document that you are given. Free response object will have many QuestionParts.
				The first question description outside of all the sub parts of the question will make up the main QuestionPart. Subsequent to that all the question parts should be extracted out as a QuestionPart. The pdf document is given between ``` and ```
				```""" + file + "```'";
		
		
		// create user message template
	    PromptTemplate userPromptTemplate = new PromptTemplate("""
	      Generate the Free Response object out of the pdf document that you are given. Free response object will have many QuestionParts.
				The first question description outside of all the sub parts of the question will make up the main QuestionPart. Subsequent to that all the question parts should be extracted out as a QuestionPart. The pdf document to use is the text delimited by triple backquotes
	      
	      ```Text:{content}```

	      """, Map.of("content", file));
	    Message userMessage = userPromptTemplate.createMessage();
	    
	   Flux<String> responseStream = chatClient.stream(userMessage);
	    responseStream
	        .doOnNext(content -> beanOutputConverter.convert(content.trim()))
	        .blockLast();


		long start = System.currentTimeMillis();

		System.out.println("VertexAI Gemini call took " + (System.currentTimeMillis() - start) + " ms");
	}

}
