package com.mbcc.quiz.service;

/*
 * Copyright 2024 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.google.cloud.vertexai.Transport;
import com.google.cloud.vertexai.VertexAI;
import org.springframework.ai.chat.prompt.Prompt;
import org.springframework.ai.vertexai.gemini.VertexAiGeminiChatModel;
import org.springframework.ai.vertexai.gemini.VertexAiGeminiChatOptions;

public class SimpleChatExample {

  public static void main(String[] args) {

    VertexAI vertexAI = new VertexAI.Builder()
        .setLocation(Constants.LOCATION)
        .setProjectId(Constants.PROJECT_ID)
        .setTransport(Transport.REST)
        .build();

    var geminiChatModel = new VertexAiGeminiChatModel(vertexAI,
        VertexAiGeminiChatOptions.builder()
            .model("gemini-pro")
            .temperature(0.2)
            .topK(5)
            .topP(0.95)
            .build());

    String prompt = """
    		You are a personal tutor helping student learn Java Programming. 

I want you to act as my Java instructor. Don't just give me the solution. Instead, guide the student step-by-step towards understanding and solving the problem themselves.

For example, start with a high-level explanation of the problem and the approach we should take. Then, give me one hint or question at a time to help them figure out the next part of the solution on their own.

 Student will provide their attempts at each step, and you can give them feedback, corrections, and further guidance. You have to work together with the student to solve this!"


Problem:
"Write a Java program to find the factorial of a given number."


Here's the solution:

public class Factorial {
    public static void main(String[] args) {
        int num = 5;
        long factorial = 1;
        for(int i = 1; i <= num; ++i)
            factorial *= i;
        System.out.println("Factorial of " + num + " = " + factorial);
    }
}
Can you guide me through solving this problem step-by-step? 

Student initial code:
public class Factorial {
public void main(String args){

    		""";

    // call Gemini in VertexAI
    long start = System.currentTimeMillis();
    System.out.println("GEMINI: " + geminiChatModel
        .call(new Prompt(prompt))
        .getResult().getOutput().getContent());
    System.out.println(
        "VertexAI Gemini call took " + (System.currentTimeMillis() - start) + " ms");
  }
}
