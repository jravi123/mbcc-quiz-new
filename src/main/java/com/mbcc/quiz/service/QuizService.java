package com.mbcc.quiz.service;

import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mbcc.quiz.entity.Conversation;
import com.mbcc.quiz.entity.FreeResponse;
import com.mbcc.quiz.entity.FreeResponseRepository;
import com.mbcc.quiz.entity.Ques;
import com.mbcc.quiz.entity.QuesRepository;
import com.mbcc.quiz.entity.QuestionPart;
import com.mbcc.quiz.entity.Quiz;
import com.mbcc.quiz.entity.QuizRepository;
import com.mbcc.quiz.entity.Rating;
import com.mbcc.quiz.entity.RatingRepository;
import com.mbcc.quiz.entity.RcFadLog;
import com.mbcc.quiz.entity.RcFadLogRespository;
import com.mbcc.quiz.entity.Score;
import com.mbcc.quiz.entity.ScoreRepository;
import com.mbcc.quiz.entity.Topic;
import com.mbcc.quiz.entity.TopicRepository;
import com.mbcc.quiz.entity.Unit;
import com.mbcc.quiz.entity.UnitRepository;
import com.mbcc.quiz.entity.UserConversation;
import com.mbcc.quiz.entity.UserConversationRepository;

@RestController
@CrossOrigin(origins = "*")
public class QuizService {

    QuizRepository quizRepository;
    QuesRepository quesRepository;
    TopicRepository topicRepository;
    RatingRepository ratingRepository;
    ScoreRepository scoreRepository;
    UnitRepository unitRepository;
    FreeResponseRepository freeResponseRepository;
    VertexAIAgent vertexAIAgent;
    RcFadLogRespository rcfadLogRespository;
    JavaInstructorAgent javaInstructorAgent;
	UserConversationRepository userConversationRepo;

    @GetMapping("/quiz/{id}")
    Quiz getQuiz(@PathVariable("id") String id) {
        Optional<Quiz> quizO = quizRepository.findById(id);


        if (quizO.isPresent()) {
            List<Topic> topics = topicRepository.findTopicByName(quizO.get().getTopic());
            quizO.get().setLanguage(topics.get(0).getLanguage());
            final Quiz quiz = quizO.get();
            final List<String> questionIds = quiz.getqIds();
            quiz.setQuestions(quesRepository.findAllById(questionIds));
            return quiz;
        }
        return null;
    }


    @GetMapping("/genQuiz/{topic}/{unit}")
    Quiz getQuiz(@PathVariable("topic") String topic, @PathVariable("unit") String unitName) {

        List<Unit> unit = unitRepository.findByTopicAndName(topic, unitName);

        List<Ques> questions = new ArrayList<Ques>();

        if(unit.size() > 0) {
            List<String> subTopics = unit.get(0).getSubTopics();

            for (String subTopic : subTopics) {
                questions.addAll(quesRepository.findByTopicsAndSubtopic(topic, subTopic));
            }


            Collections.shuffle(questions); // TODO add sublist

            Quiz quiz = new Quiz();

            List<Topic> topics = topicRepository.findTopicByName(topic);
            quiz.setLanguage(topics.get(0).getLanguage());
            quiz.setQuestions(questions);

            // TODO, add userId to the Id
            quiz.setId("generated");

            // TODO

            return quiz;
        }
        return null;
    }


    @GetMapping("/quizAll")
    Iterable<Quiz> getQuiz() {
        return quizRepository.findAll();
    }
    

    @PostMapping("/getQuizzesForTopic")
    Iterable<Quiz> getQuizzesForTopic(@RequestParam String topic, @RequestParam String userId) {

        System.out.println("topic==" + topic + ", userId==" + userId);
        Iterable<Quiz> quizzes = this.quizRepository.findByTopicOrderByDisplayOrder(topic);

        // TODO remove hardcoded userId
        List<Score> scores = this.scoreRepository.findByTopicAndUserId(topic, userId);

        int topRecords = scores.size() < 5? scores.size():5;
        scores = scores.stream().sorted((o1, o2) -> o1.getDate().after(o2.getDate())? -1: 1).collect(toList()).subList(0, topRecords);

        for(Quiz quiz: quizzes){
            List<Score> collected = scores.stream()
                    .filter(score -> quiz.getqIds().contains(score.getQid()))
                    .collect(toList());

            double sum = collected.stream().mapToDouble(a -> a.getScore())
                    .sum();
            if(collected.size() > 0){
                quiz.setScore((int)sum/collected.size()*100);
                quiz.setQuizTaken(true);
            }
        }
        return quizzes;
    }



    @GetMapping("/question/{id}")
    Optional<Ques> getQuestion(@PathVariable("id") String id){
        return this.quesRepository.findById(id);
    }


    @GetMapping("/allTopics")
    Iterable<Topic> getTopics() {
        return topicRepository.findAll();
    }

    @GetMapping("/topic/{id}")
    Optional<Topic> getTopic(@PathVariable("id") Long id){
        return this.topicRepository.findById(id);
    }

    @PostMapping("/saveQuestion")
    String saveQuestion(@RequestBody Ques q){
        if(q.getAttachedQuiz() != null && q.getAttachedQuiz().size() == 0){
            q.setAttachedQuiz(null);
        }
        if(q.getModified() == null) {
        	q.setModified(new Date());
        }
        this.quesRepository.save(q);
        return q.getId();
    }

    @PostMapping("/saveScore")
    void saveScore(@RequestBody List<Score> scores){
        this.scoreRepository.saveAll(scores);
    }

    @PostMapping("/deleteQuestion")
    void deleteQuestion(@RequestBody Ques q){
        this.quesRepository.delete(q);
    }

    @PostMapping("/saveTopic")
    Topic saveTopic(@RequestBody Topic q){
        return this.topicRepository.save(q);
    }

    @PostMapping("/saveRating")
    Rating saveRating(@RequestBody Rating r){
        return this.ratingRepository.save(r);
    }


    @PostMapping("/saveQuiz")
    Quiz saveQuiz(@RequestBody Quiz q){
        ArrayList<String> qIds = q.getqIds();
        Iterable<Ques> questions = this.quesRepository.findAllById(qIds);
        for(Ques ques: questions){
            if(!ques.getAttachedQuiz().contains(q.getId())){
                ques.getAttachedQuiz().add(q.getId());
                this.quesRepository.save(ques);
            }
        }
        return this.quizRepository.save(q);
    }


    @GetMapping("/questions-not-attached")
    Iterable<Ques> getQuestionsNotAttached() {
        return quesRepository.findByAttachedQuizNull();
    }
    
    @GetMapping("/free-response/{id}")
    FreeResponse getFreeResponse(@PathVariable("id") Long id) {
        Optional<FreeResponse> freeResponse = freeResponseRepository.findById(id);
        FreeResponse freeResponse2 = freeResponse.get();
        QuestionPart[] qparts = freeResponse2.getQparts();
        for (QuestionPart questionPart : qparts) {
        	questionPart.setPartKey(null);
		}

        return freeResponse2;
    }
    
    @PostMapping("/saveFreeResponse")
    FreeResponse saveFreeResponse(@RequestBody FreeResponse fr){
    
    	//TODO
     //	fr.setEnteredBy(userId)
    	if(fr.getDateAdded() == null)
    		fr.setDateAdded(new Date());
    	fr.setDateEdited(new Date());
    	System.out.println(fr);
        return this.freeResponseRepository.save(fr);
    }
    
    @PostMapping("/ai")
    String getAIHelp(@RequestBody List<Object> input) {
    	
    	String userId = (String) input.get(0);
    	String currentQuestion = (String) input.get(1);
    	ObjectMapper mapper = new ObjectMapper();
    	FreeResponse freeresponse = mapper.convertValue(input.get(2), FreeResponse.class);
    	int qPart = (Integer)input.get(3);
    	
    	List<Conversation> conversations =  mapper.convertValue(input.get(4), new TypeReference<List<Conversation>>() {});
    	
    	return this.javaInstructorAgent.getAIHelp(userId, currentQuestion, freeresponse, qPart, conversations);
    }
    
    @PostMapping("/genAIHelp")
    String genAIHelp(@RequestParam String userId,@RequestParam String currentQuestion, 
    		@RequestParam String qId, @RequestParam String qPart, 
    		@RequestBody List<Conversation> conversations) throws IOException {
    	
    	UserConversation userConversation = new UserConversation();
    	userConversation.setId(userId);
    	userConversation.setConversations(conversations);
    	
    	return vertexAIAgent.apcsaHelp(userId, userConversation, currentQuestion, qId, qPart);
    }
    
    @PostMapping("/saveUserAIFeedback")
    void saveFeedback(@RequestBody List<Object> input) {
    	
    	String userId = (String) input.get(0);
    	ObjectMapper mapper = new ObjectMapper();
    	List<Conversation> conversations =  mapper.convertValue(input.get(1), new TypeReference<List<Conversation>>() {});
    	
    	UserConversation userConversation = new UserConversation();
    	userConversation.setId(userId);
    	userConversation.setConversations(conversations);
    	this.userConversationRepo.save(userConversation);
    }
    
    


// RCFAT related
    
    @PostMapping("/logRcFad")
    Iterable<RcFadLog> logRcFad(@RequestBody List<RcFadLog> rcl){
    
       return this.rcfadLogRespository.saveAll(rcl);
    }
    
    
    @GetMapping("/logRcFad-delete-all")
    void deleteLogs(){
    	this.rcfadLogRespository.deleteAll();
    }

    QuizService(QuizRepository quizRepository, QuesRepository questionRepository, TopicRepository topicRepository,
                RatingRepository ratingRepository, ScoreRepository scoreRepository, 
                UnitRepository unitRepository, FreeResponseRepository freeResponseRepository, 
                VertexAIAgent vertexAIAgent, RcFadLogRespository rcfadRepository, 
                JavaInstructorAgent javaInstructorAgent, UserConversationRepository userConversationrepo) {
        this.quizRepository = quizRepository;
        this.quesRepository = questionRepository;
        this.topicRepository = topicRepository;
        this.ratingRepository = ratingRepository;
        this.scoreRepository = scoreRepository;
        this.unitRepository = unitRepository;
        this.freeResponseRepository = freeResponseRepository;
        this.rcfadLogRespository = rcfadRepository;
        this.vertexAIAgent = vertexAIAgent;
        this.javaInstructorAgent = javaInstructorAgent;
        this.userConversationRepo = userConversationrepo;
    }
}


