package com.mbcc.quiz.service;

import static com.mbcc.quiz.service.Constants.LOCATION;
import static com.mbcc.quiz.service.Constants.PROJECT_ID;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.ai.chat.client.ChatClient;
import org.springframework.ai.chat.client.advisor.MessageChatMemoryAdvisor;
import org.springframework.ai.chat.client.advisor.PromptChatMemoryAdvisor;
import org.springframework.ai.chat.memory.InMemoryChatMemory;
import org.springframework.ai.chat.messages.AssistantMessage;
import org.springframework.ai.chat.messages.Message;
import org.springframework.ai.chat.messages.SystemMessage;
import org.springframework.ai.chat.messages.UserMessage;
import org.springframework.ai.chat.prompt.Prompt;
import org.springframework.ai.chat.prompt.PromptTemplate;
import org.springframework.ai.vertexai.gemini.VertexAiGeminiChatModel;
import org.springframework.ai.vertexai.gemini.VertexAiGeminiChatOptions;
import org.springframework.stereotype.Service;

import com.google.cloud.vertexai.VertexAI;
import com.mbcc.quiz.entity.Conversation;
import com.mbcc.quiz.entity.FreeResponse;
import com.mbcc.quiz.entity.FreeResponseRepository;
import com.mbcc.quiz.entity.GenAISettings;
import com.mbcc.quiz.entity.GenAISettingsRespository;
import com.mbcc.quiz.entity.PromptRepository;
import com.mbcc.quiz.entity.QuestionPart;
import com.mbcc.quiz.entity.UserConversation;
import com.mbcc.quiz.entity.UserConversationRepository;

@Service
public class VertexAIAgent {

	private GenAISettingsRespository genAISettingsRepository;
	private PromptRepository promptRespository;
	private FreeResponseRepository freeResponseRepository;

	static HashMap<String, InMemoryChatMemory> chats = new HashMap<String, InMemoryChatMemory>();
	static HashMap<Long, FreeResponse> freeResponses = new HashMap<Long, FreeResponse>();
	static HashMap<Long, Prompt> prompts = new HashMap<Long, Prompt>();
	private GenAISettings genAISettings;
	private String modelName;
	private double temparature;
	private int topK;
	private double topP;
	private UserConversationRepository userMessageRepository;
	private HtmlService htmlservice;
	private static final int CACHE_SIZE = 30;

	public String apcsaHelp(String userId, UserConversation userConversation, String currentQuestion, String qId,
			String qPart) {

		FreeResponse freeResponse = getFreeResponse(qId);
		QuestionPart[] qparts = freeResponse.getQparts();
		QuestionPart questionPart = qparts[Integer.parseInt(qPart)];

		Prompt prompt = getPrompt(freeResponse);

		InMemoryChatMemory chatMemory = chats.get(userId);

		if (chatMemory == null) {
			chatMemory = new InMemoryChatMemory();
			if (chats.size() > CACHE_SIZE) {
				chats.clear();
			}
			chats.put(userId, chatMemory);

			chatMemory.add("problem", new UserMessage("problem:" + freeResponse.getDescription()));
			chatMemory.add("qpart-code", new UserMessage("code to complete:" + questionPart.getCode()));
			chatMemory.add("qpart-description",
					new UserMessage("question description:" + questionPart.getDescription()));
			chatMemory.add("qpart-solution",
					new UserMessage("solution for the code to complete section:" + questionPart.getSolution()));

			// add conversations only if new
			if (userConversation != null) {
				List<Message> messages = new ArrayList<Message>();
				List<Conversation> conversations = userConversation.getConversations();

				for (Conversation conversation : conversations) {
					messages.add(new UserMessage(conversation.getUserAsked()));
					messages.add(new AssistantMessage(conversation.getAiSaid()));
				}

				chatMemory.add("prir conversations with the student so far", messages);
			}
		}

		try (VertexAI vertexAi = new VertexAI(PROJECT_ID, LOCATION);) {

			var geminiChatModel = new VertexAiGeminiChatModel(vertexAi, VertexAiGeminiChatOptions.builder()
					.model(modelName).temperature(temparature).topK(topK).topP(topP).build());

			var chatClient = ChatClient.builder(geminiChatModel).defaultSystem(prompt.getContents())
					.defaultAdvisors(new MessageChatMemoryAdvisor(chatMemory)).build();

			// String userMessage = "refer student code```" + currentQuestion + "``` Now
			// asnwer the last question that the student has asked.";

			String userMessage = currentQuestion;
			String aiResponse = chatClient.prompt().user(userMessage).call().content();
			aiResponse = this.htmlservice.markdownToHtml(aiResponse);
			userConversation.getConversations().add(new Conversation(currentQuestion, aiResponse));
			this.userMessageRepository.save(userConversation);

			return aiResponse;
		} catch (Exception e) {
			System.out.print(e);
		}
		return null;

	}

	private Prompt getPrompt(FreeResponse freeResponse) {

		Prompt prompt = prompts.get(freeResponse.getPromptId());
		if (prompt == null) {
			if (prompts.size() > CACHE_SIZE) {
				prompts.clear();
			}
			Optional<com.mbcc.quiz.entity.Prompt> promptO = promptRespository.findById(freeResponse.getPromptId());
			prompt = new Prompt(new PromptTemplate(promptO.get().getPrompt()).createMessage());
		}
		return prompt;
	}

	private FreeResponse getFreeResponse(String qId) {
		FreeResponse freeResponse = freeResponses.get(Long.parseLong(qId));
		if (freeResponse == null) {
			if (freeResponses.size() > CACHE_SIZE) {
				freeResponses.clear();
			}
			freeResponse = freeResponseRepository.findById(Long.parseLong(qId)).get();
		}
		return freeResponse;
	}

	public void setupGenAIModel() {
		Iterable<GenAISettings> allSettings = genAISettingsRepository.findBySelected(true);
		genAISettings = allSettings.iterator().next();

		modelName = genAISettings.getModelName();

		temparature = genAISettings.getTemperature();
		topK = (int) genAISettings.getTopK();
		topP = genAISettings.getTopP();
	}

	VertexAIAgent(GenAISettingsRespository respository, PromptRepository promptRepo,
			FreeResponseRepository freeResponseRepository, UserConversationRepository userMessageRepo,
			HtmlService htmlservice) {
		this.genAISettingsRepository = respository;
		this.promptRespository = promptRepo;
		this.freeResponseRepository = freeResponseRepository;
		this.userMessageRepository = userMessageRepo;
		this.htmlservice = htmlservice;

		setupGenAIModel();

	}
}

// https://console.cloud.google.com/vertex-ai/studio/prompt-gallery/Generate%20Java%20classes?inv=1&invt=AbmETQ&project=mbcc-quiz
