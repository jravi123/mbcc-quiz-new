package com.mbcc.quiz.service;

/*
 * Copyright 2024 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.util.List;
import org.springframework.ai.document.Document;
import org.springframework.ai.reader.ExtractedTextFormatter;
import org.springframework.ai.reader.JsonReader;
import org.springframework.ai.reader.TextReader;
import org.springframework.ai.reader.pdf.PagePdfDocumentReader;
import org.springframework.ai.reader.pdf.config.PdfDocumentReaderConfig;
import org.springframework.ai.transformer.splitter.TokenTextSplitter;
import org.springframework.core.io.ClassPathResource;

public class DocumentProcessingExample {
  public static void main(String[] args) {
    

    // Read PDF documents
    //--------------------
    PagePdfDocumentReader pdfReader = new PagePdfDocumentReader("classpath:/apcsa/ap24-frq-comp-sci-a.pdf",
        PdfDocumentReaderConfig.builder()
            .withPageTopMargin(0)
            .withPageExtractedTextFormatter(ExtractedTextFormatter.builder()
                .withNumberOfTopTextLinesToDelete(0)
                .build())
            .withPagesPerDocument(1)
            .build());
    List<Document> pdfDocument = pdfReader.read();
    for(Document document : pdfDocument)
      System.out.printf("Read PDF document %s ... with length %d\n",
          document.getContent().trim().substring(0, 50),
          document.getContent().length());


    System.out.println(pdfDocument.get(0).getContent());
    System.out.println(pdfDocument.size());
  }
}