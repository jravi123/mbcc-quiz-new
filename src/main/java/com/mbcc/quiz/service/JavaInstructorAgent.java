package com.mbcc.quiz.service;

import static com.mbcc.quiz.service.Constants.LOCATION;
import static com.mbcc.quiz.service.Constants.PROJECT_ID;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.helpers.FormattingTuple;
import org.slf4j.helpers.MessageFormatter;
import org.springframework.ai.chat.prompt.Prompt;
import org.springframework.ai.vertexai.gemini.VertexAiGeminiChatModel;
import org.springframework.ai.vertexai.gemini.VertexAiGeminiChatModel.ChatModel;
import org.springframework.ai.vertexai.gemini.VertexAiGeminiChatOptions;
import org.springframework.stereotype.Service;

import com.google.cloud.vertexai.VertexAI;
import com.mbcc.quiz.entity.Conversation;
import com.mbcc.quiz.entity.FreeResponse;
import com.mbcc.quiz.entity.QuestionPart;
import com.mbcc.quiz.entity.UserConversation;
import com.mbcc.quiz.entity.UserConversationRepository;

@Service
public class JavaInstructorAgent {
	
	private UserConversationRepository userConversationRepo;

	private final VertexAiGeminiChatModel chatModel;

	JavaInstructorAgent(UserConversationRepository userMessageRepo, HtmlService htmlService) {
		VertexAI vertexApi = new VertexAI(PROJECT_ID, LOCATION);

		chatModel = new VertexAiGeminiChatModel(vertexApi,
				VertexAiGeminiChatOptions.builder().model(ChatModel.GEMINI_1_5_FLASH).temperature(0.4).build());
		this.userConversationRepo = userMessageRepo;
	}

	
	public String getHelp(String message) {
		return this.chatModel.call(
			    new Prompt(message)).getResult().getOutput().getContent();
	}


	public String getAIHelp(String userId, String partialCode, FreeResponse freeResponse, int qPart,
			List<Conversation> conversations) {
		
		StringBuilder conversation = new StringBuilder();
		for (int i = 0; i < conversations.size()-1; i++) {
			conversation.append("I asked:" + conversations.get(i).getUserAsked());
			conversation.append("You said:" + conversations.get(i).getAiSaid());
		}
		
		String message = """
    			You are a Java Instructor helping me, a high school student solve this problem. 
    			Here is the problem:{}
    			I'm trying to solve this part:{}
    			The solution for that is:
    			```{}```
    			Help me solve this based on the given solution.
    			DO NOT INVENT your own solution. 
    			Help me solve this problem one statement at a time.
    			Do not give me the complete solution at once. 
    			You may show me one statement after interacting with you at least 4 times.
    			Give me hints on how to solve the problem based on the given solution.
    			I REPEAT do not INVENT your own solution and DO NOT assume anything else other than what is given as the problem.
    			I may use other variables than the given solution although. My logic may be slightly different from the given solution. 
    			This is fine as long as the functionality  of my solution matches the given solution.
    			You may also get a handwritten solution from me as a picture. 
    			Please let me know which parts of my solution is incorrect.
    			My prior conversation with you are given below;
    			{}
    			My solution (could be partial also) is:{}
    			Now I am asking:{}
    			""";
    	
      	
    	QuestionPart questionPart = freeResponse.getQparts()[qPart];
    	
    	String userAsked = conversations.get(conversations.size()-1).getUserAsked();
    	
    	List<String> arguments = new ArrayList<String>();
    	arguments.add(freeResponse.getDescription());
    	arguments.add(questionPart.getDescription());
    	arguments.add(questionPart.getSolution());
    	arguments.add(conversation.toString());
    	arguments.add(partialCode);
    	arguments.add(userAsked);
    	
   
    	String fullMessage = MessageFormatter.arrayFormat(message, arguments.toArray()).getMessage();
    	System.out.println(fullMessage);
    	
    	UserConversation userConversation = new UserConversation();
    	userConversation.setId(userId);
    	userConversation.setConversations(conversations);
    	this.userConversationRepo.save(userConversation);
    	
    	String aiResponse = HtmlService.markdownToHtml(this.chatModel.call(
			    new Prompt(fullMessage)).getResult().getOutput().getContent());
    	
    	userConversation.getConversations().get(conversations.size()-1).setAiSaid(aiResponse);
    	this.userConversationRepo.save(userConversation);
    	
    	return aiResponse;
	}
	
	
	public static void main(String[] args) {
		String message = """
    			You are a Java Instructor helping me, a high school student solve this problem. 
    			Here is the problem:{}
    			I'm trying to solve this part:{}
    			The solution for that is:
    			```{}```
    			I have partially completed the solution:
    			```{}```
    			Help me solve this based on the given solution.
    			DO NOT INVENT your own solution. 
    			Help me solve this problem one statement at a time.
    			Do not give me the complete solution at once. You may show me one statement after interacting with you at least 4 times.
    			My prior conversation with you is:
    			
    			Now I am asking:{}
    			""";
    	
      	

    	
    	List<String> arguments = new ArrayList<String>();
    	arguments.add("ABC");
    	arguments.add("XYZ");
    	arguments.add("!@#");
    	arguments.add("123");
    	arguments.add("0000");
    	
   
    	String fullMessage = MessageFormatter.arrayFormat(message, arguments.toArray()).getMessage();
    	System.out.println(fullMessage);
	}

}
