package com.mbcc.quiz.entity;

import org.springframework.data.annotation.Id;

import com.google.cloud.datastore.Key;
import com.google.cloud.spring.data.datastore.core.mapping.Entity;
import com.google.cloud.spring.data.datastore.core.mapping.Unindexed;

@Entity(name = "QuestionPart")
public class QuestionPart {

	@Id
	Key partKey;
	
	private String solution;
	
    @Unindexed
	private String description;
	private String code;

	

	public String getSolution() {
		return solution;
	}

	public void setSolution(String solution) {
		this.solution = solution;
	}

	public Key getPartKey() {
		return partKey;
	}

	public void setPartKey(Key partKey) {
		this.partKey = partKey;
	}


	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "QuestionPart [partKey=" + partKey + ", solution=" + solution + ", description=" + description
				+ ", code=" + code + "]";
	}



}
