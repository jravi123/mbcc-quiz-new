package com.mbcc.quiz.entity;

import com.google.cloud.spring.data.datastore.repository.DatastoreRepository;

import java.util.List;
import java.util.Optional;


public interface QuizRepository extends DatastoreRepository<Quiz, String> {



    List<Quiz> findByTopicOrderByDisplayOrder(String topic);

    List<Quiz> findByqIds(String qIds);
}
