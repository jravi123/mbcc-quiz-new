package com.mbcc.quiz.entity;

import com.google.cloud.spring.data.datastore.repository.DatastoreRepository;

import java.util.List;


public interface UnitRepository extends DatastoreRepository<Unit, Long> {

    List<Unit> findByTopicAndName(String topic, String name);
}

