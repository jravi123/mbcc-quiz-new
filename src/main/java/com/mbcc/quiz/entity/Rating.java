package com.mbcc.quiz.entity;

import com.google.cloud.spring.data.datastore.core.mapping.Entity;

import java.util.Date;

import org.springframework.data.annotation.Id;

@Entity(name = "Rating")
public class Rating {
	
    @Id
    private Long id;
	private int rating;
	private String feedback;
	private boolean correct;
	private String questionId;
	private String username;
	private Date date = new Date();
	
	public Date getDate() {
		return date;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating + 1;
	}
	public String getFeedback() {
		return feedback;
	}
	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}
	public boolean isCorrect() {
		return correct;
	}
	public void setCorrect(boolean correct) {
		this.correct = correct;
	}
	public String getQuestionId() {
		return questionId;
	}
	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
}
