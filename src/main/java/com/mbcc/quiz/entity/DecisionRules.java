package com.mbcc.quiz.entity;

import org.springframework.data.annotation.Id;

import com.google.cloud.spring.data.datastore.core.mapping.Entity;

@Entity(name = "DecisionRules")
public class DecisionRules {
	
	 @Id
	 private String id;
	 private String qId;
	 private String criteria;
	 private String decisionRules;
	 private float score;

}
