package com.mbcc.quiz.entity;


import com.google.cloud.spring.data.datastore.core.mapping.Entity;
import com.google.cloud.spring.data.datastore.core.mapping.Unindexed;
import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity(name = "Ques")
public class Ques {


    @Id
    private String id;

    private String author;

    private String question;

    private Date modified;

    private String source;

    @Unindexed
    private String image;

    private String code;

    private List<String> attachedQuiz;

    private String language;

    private String type;

    private List<String> answerChoices = new ArrayList<String>();

    private List<String> choicesExplained = new ArrayList<String>();

    private List<String> correctAnswers;

    private String explain;


    private List<String> topics;

    private String subtopic;

    private Float averageRating;

    private short difficultLevel;

    private Integer totalRatings;


    public List<String> getTopics() {
        return topics;
    }

    public void setTopics(List<String> topics) {
        this.topics = topics;
    }


    public short getDifficultLevel() {
        return difficultLevel;
    }

    public void setDifficultLevel(short difficultLevel) {
        this.difficultLevel = difficultLevel;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public List<String> getAttachedQuiz() {
        return attachedQuiz;
    }

    public void setAttachedQuiz(List<String> attachedQuiz) {
        this.attachedQuiz = attachedQuiz;
    }

    public void setAverageRating(float averageRating) {
        this.averageRating = averageRating;
    }


    public String getSubtopic() {
        return subtopic;
    }

    public void setSubtopic(String subtopic) {
        this.subtopic = subtopic;
    }

    public List<String> getChoicesExplained() {
        return choicesExplained;
    }

    public void setChoicesExplained(List<String> choicesExplained) {
        this.choicesExplained = choicesExplained;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }


    public String getAuthor() {
        return author;
    }
    public void setAuthor(String author) {
        this.author = author;
    }

    public List<String> getCorrectAnswers() {
        return correctAnswers;
    }

    public void setCorrectAnswers(List<String> correctAnswers) {
        this.correctAnswers = correctAnswers;
    }

    public Float getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(Float averageRating) {
        this.averageRating = averageRating;
    }

    public Integer getTotalRatings() {
        return totalRatings;
    }

    public void setTotalRatings(Integer totalRatings) {
        this.totalRatings = totalRatings;
    }

    public String getQuestion() {
        return question;
    }
    public void setQuestion(String question) {
        this.question = question;
    }
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code ;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    public List<String> getAnswerChoices() {
        return answerChoices;
    }

    public void setAnswerChoices(List<String> answerChoices) {
        this.answerChoices = answerChoices;
    }

    public void addAnswerChoice(String answer){
        this.answerChoices.add(answer);
    }

    public String getExplain() {
        return explain;
    }
    public void setExplain(String explain) {
        this.explain = explain;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


    public Ques() {
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    @Override
    public String toString() {
        return "Ques{" +
                "id='" + id + '\'' +
                ", author='" + author + '\'' +
                ", question='" + question + '\'' +
                ", image='" + image + '\'' +
                ", code='" + code + '\'' +
                ", type='" + type + '\'' +
                ", answerChoices=" + answerChoices +
                ", correctAnswers=" + correctAnswers +
                ", explain='" + explain + '\'' +
                ", subtopic='" + subtopic + '\'' +
                ", averageRating=" + averageRating +
                ", totalRatings=" + totalRatings +
                '}';
    }
}
