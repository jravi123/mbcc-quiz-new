package com.mbcc.quiz.entity;

import com.google.cloud.spring.data.datastore.repository.DatastoreRepository;
import org.springframework.data.domain.Sort;

import java.util.Date;
import java.util.List;


public interface ScoreRepository extends DatastoreRepository<Score, String> {

    List<Score> findByTopicAndUserId(String topic, String userId);



}