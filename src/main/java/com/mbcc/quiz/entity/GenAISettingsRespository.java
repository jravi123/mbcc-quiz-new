package com.mbcc.quiz.entity;

import com.google.cloud.spring.data.datastore.repository.DatastoreRepository;

public interface GenAISettingsRespository extends DatastoreRepository<GenAISettings, Long> {

	Iterable<GenAISettings> findBySelected(boolean b);

}
