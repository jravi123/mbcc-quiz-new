package com.mbcc.quiz.entity;

import java.util.List;

import com.google.cloud.spring.data.datastore.repository.DatastoreRepository;



public interface FreeResponseRepository extends DatastoreRepository<FreeResponse, Long> {

	 List<FreeResponse> findByYear(String year);

}
