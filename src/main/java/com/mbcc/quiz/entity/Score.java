package com.mbcc.quiz.entity;

import com.google.cloud.spring.data.datastore.core.mapping.Entity;
import org.springframework.data.annotation.Id;


import java.util.Date;

@Entity(name = "Score")
public class Score {

	@Id
	private Long id;
	
	private String userId;
	
	private Date submittedOn = new Date();
	
	public Date getDate() {
		return submittedOn;
	}

	public void setDate(Date date) {
		this.submittedOn = date;
	}

	private double score;
	
	private String subTopic;

	private String qid;

	private String topic;

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	private short difficultyLevel;

	public short getDifficultyLevel() {
		return difficultyLevel;
	}

	public void setDifficultyLevel(short difficultyLevel) {
		this.difficultyLevel = difficultyLevel;
	}

	public String getSubTopic() {
		return subTopic;
	}

	public void setSubTopic(String subTopic) {
		this.subTopic = subTopic;
	}

	public String getQid() {
		return qid;
	}

	public void setQid(String qid) {
		this.qid = qid;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	@Override
	public String toString() {
		return "Score{" +
				"id=" + id +
				", userId='" + userId + '\'' +
				", submittedOn=" + submittedOn +
				", score=" + score +
				", subTopic='" + subTopic + '\'' +
				", qid='" + qid + '\'' +
				", topic='" + topic + '\'' +
				", difficultyLevel=" + difficultyLevel +
				'}';
	}
}
