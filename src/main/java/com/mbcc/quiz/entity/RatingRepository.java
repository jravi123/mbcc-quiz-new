package com.mbcc.quiz.entity;

import com.google.cloud.spring.data.datastore.repository.DatastoreRepository;

import java.util.Date;
import java.util.List;


public interface RatingRepository extends DatastoreRepository<Rating, String> {


}

