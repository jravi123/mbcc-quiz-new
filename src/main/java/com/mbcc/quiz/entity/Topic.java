package com.mbcc.quiz.entity;


import com.google.cloud.spring.data.datastore.core.mapping.Entity;
import org.springframework.data.annotation.Id;

import java.util.List;

@Entity(name = "Topic")
public class Topic {

	@Id
	private Long id;

	private String name;

	private String language;

	private List<String>  subTopics;

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getSubTopics() {
		return subTopics;
	}

	public void setSubTopics(List<String> subTopics) {
		this.subTopics = subTopics;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
