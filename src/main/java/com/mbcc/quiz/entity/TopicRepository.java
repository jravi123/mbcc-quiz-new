package com.mbcc.quiz.entity;

import com.google.cloud.spring.data.datastore.repository.DatastoreRepository;

import java.util.List;


public interface TopicRepository extends DatastoreRepository<Topic, Long> {

    public List<Topic> findTopicByName(String name);
}

