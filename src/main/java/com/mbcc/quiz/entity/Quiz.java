package com.mbcc.quiz.entity;

import com.google.cloud.spring.data.datastore.core.mapping.Entity;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;

import java.util.ArrayList;

@Entity(name = "Quiz")
public class Quiz {

    @Id
    private String id;

    private String topic;

    private String author;

    private String quizname;

    private String rating;

    private int displayOrder;

    private double score;

    @Transient
    private boolean quizTaken;

    @Transient
    private String language;

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public boolean isQuizTaken() {
        return quizTaken;
    }

    public void setQuizTaken(boolean quizTaken) {
        this.quizTaken = quizTaken;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public int getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }


    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    private ArrayList<String> qIds = new ArrayList<String>();

    @Transient
    private Iterable<Ques> questions;


    public Iterable<Ques> getQuestions() {
        return questions;
    }

    public void setQuestions(Iterable<Ques> questions) {
        this.questions = questions;
    }


    public String getQuizname() {
        return quizname;
    }

    public void setQuizname(String quizname) {
        this.quizname = quizname;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public ArrayList<String> getqIds() {
        return qIds;
    }

    public void setqIds(ArrayList<String> qIds) {
        this.qIds = qIds;
    }


    public void addQid(String qid) {
        if (this.qIds == null) {
            qIds = new ArrayList<String>();

        }
        qIds.add(qid);
    }
}


