package com.mbcc.quiz.entity;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

// https://googlecloudplatform.github.io/spring-cloud-gcp/reference/html/datastore.html#_relationships


import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Reference;

import com.google.cloud.spring.data.datastore.core.mapping.Descendants;
import com.google.cloud.spring.data.datastore.core.mapping.Entity;
import com.google.cloud.spring.data.datastore.core.mapping.Unindexed;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;

@Entity(name = "FreeResponse")
public class FreeResponse {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	private Long id;



	@Reference
	private QuestionPart[] qparts;
	
	private int year;
	private Date dateAdded;
	private Date dateEdited;
	private String enteredBy;
	private String source;
	private String title;
	private List<DecisionRules> decisionRules;
	
	@Unindexed
	private String description;
	
	
	@Descendants
	private List<Sample> samples;

	private Long promptId;


	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Sample> getSamples() {
		return samples;
	}

	public void setSamples(List<Sample> samples) {
		this.samples = samples;
	}

	
	public List<DecisionRules> getDecisionRules() {
		return decisionRules;
	}

	public void setDecisionRules(List<DecisionRules> decisionRules) {
		this.decisionRules = decisionRules;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public Date getDateAdded() {
		return dateAdded;
	}

	public void setDateAdded(Date dateAdded) {
		this.dateAdded = dateAdded;
	}

	public Date getDateEdited() {
		return dateEdited;
	}

	public void setDateEdited(Date dateEdited) {
		this.dateEdited = dateEdited;
	}

	public String getEnteredBy() {
		return enteredBy;
	}

	public void setEnteredBy(String enteredBy) {
		this.enteredBy = enteredBy;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public QuestionPart[] getQparts() {
		return qparts;
	}

	public void setQparts(QuestionPart[] qparts) {
		this.qparts = qparts;
	}
	
	public Long getPromptId() {
		return promptId;
	}

	public void setPromptId(Long promptId) {
		this.promptId = promptId;
	}

	@Override
	public String toString() {
		return "FreeResponse [id=" + id + ", qparts=" + Arrays.toString(qparts) + ", year=" + year + ", dateAdded="
				+ dateAdded + ", dateEdited=" + dateEdited + ", enteredBy=" + enteredBy + ", source=" + source
				+ ", decisionRules=" + decisionRules + ", description=" + description + ", samples=" + samples + "]";
	}



}
