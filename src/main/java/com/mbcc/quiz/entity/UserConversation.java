package com.mbcc.quiz.entity;

import java.util.List;

import org.springframework.data.annotation.Id;

import com.google.cloud.spring.data.datastore.core.mapping.Descendants;
import com.google.cloud.spring.data.datastore.core.mapping.Entity;

@Entity(name = "UserConversation")
public class UserConversation {
	
    @Id
    private String id;
    
    @Descendants
    private List<Conversation> conversations;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<Conversation> getConversations() {
		return conversations;
	}

	public void setConversations(List<Conversation> conversations) {
		this.conversations = conversations;
	}
    

}
