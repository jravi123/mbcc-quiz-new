package com.mbcc.quiz.entity;

import org.springframework.data.annotation.Id;

import com.google.cloud.datastore.Key;
import com.google.cloud.spring.data.datastore.core.mapping.Entity;

@Entity(name = "Sample")
public class Sample {

	@Id
	Key sampleKey;
	
	private String solution;
	private float score;
	private String commentary;

	


	public Key getSampleKey() {
		return sampleKey;
	}

	public void setSampleKey(Key sampleKey) {
		this.sampleKey = sampleKey;
	}

	public String getSolution() {
		return solution;
	}

	public void setSolution(String solution) {
		this.solution = solution;
	}

	public float getScore() {
		return score;
	}

	public void setScore(float score) {
		this.score = score;
	}

	public String getCommentary() {
		return commentary;
	}

	public void setCommentary(String commentary) {
		this.commentary = commentary;
	}

}
