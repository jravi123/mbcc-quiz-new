package com.mbcc.quiz.entity;

import com.google.cloud.spring.data.datastore.repository.DatastoreRepository;

public interface PromptRepository extends DatastoreRepository<Prompt, Long> {

}
