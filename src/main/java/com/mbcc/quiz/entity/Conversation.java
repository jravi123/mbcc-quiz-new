package com.mbcc.quiz.entity;

import java.util.Date;

import org.springframework.data.annotation.Id;

import com.google.cloud.datastore.Key;
import com.google.cloud.spring.data.datastore.core.mapping.Unindexed;

public class Conversation {
	
	@Id
	Key key;
	
	private String userAsked;
	
	@Unindexed
	private String aiSaid;
	private Date date;
	private boolean helpful;

	public boolean isHelpful() {
		return helpful;
	}
	public void setHelpful(boolean helpful) {
		this.helpful = helpful;
	}
	public String getUserAsked() {
		return userAsked;
	}
	public void setUserAsked(String userAsked) {
		this.userAsked = userAsked;
	}
	public String getAiSaid() {
		return aiSaid;
	}
	public void setAiSaid(String aiSaid) {
		this.aiSaid = aiSaid;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	public Conversation() {
		this.date = new Date();
	}

	public Conversation(String userAsked, String aiResponse) {
		this.date = new Date();
		this.userAsked = userAsked;
		this.aiSaid = aiResponse;
	}

	public Key getKey() {
		return key;
	}
	public void setKey(Key key) {
		this.key = key;
	}
	
	
}
