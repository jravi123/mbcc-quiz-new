package com.mbcc.quiz.entity;

import com.google.cloud.spring.data.datastore.repository.DatastoreRepository;

import java.util.Date;
import java.util.List;


public interface QuesRepository extends DatastoreRepository<Ques, String> {

    List<Ques> findByTopics(String topic);

    List<Ques> findByModifiedAfter(Date date);

    List<Ques> findByAttachedQuizNull();

    List<Ques>  findByTopicsAndSubtopic(String topic, String subTopic);

}

