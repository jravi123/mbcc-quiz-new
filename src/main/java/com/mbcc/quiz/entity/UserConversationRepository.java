package com.mbcc.quiz.entity;

import com.google.cloud.spring.data.datastore.repository.DatastoreRepository;

public interface UserConversationRepository extends DatastoreRepository<UserConversation, String> { 

}
