package com.mbcc.quiz.entity;

import org.springframework.data.annotation.Id;

import com.google.cloud.spring.data.datastore.core.mapping.Entity;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;


@Entity(name = "GenAISettings")
public class GenAISettings {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	private Long id;
	
	
	private String modelName;
	private String supportedGenerationMethods;
	private int outputTokenLimit;
	private float temperature;
	private float topK;
	private float topP;
	private float version;
	private boolean selected;
	
	public String getModelName() {
		return modelName;
	}
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
	public String getSupportedGenerationMethods() {
		return supportedGenerationMethods;
	}
	public void setSupportedGenerationMethods(String supportedGenerationMethods) {
		this.supportedGenerationMethods = supportedGenerationMethods;
	}
	public int getOutputTokenLimit() {
		return outputTokenLimit;
	}
	public void setOutputTokenLimit(int outputTokenLimit) {
		this.outputTokenLimit = outputTokenLimit;
	}
	public float getTemperature() {
		return temperature;
	}
	public void setTemperature(float temperature) {
		this.temperature = temperature;
	}
	public float getTopK() {
		return topK;
	}
	public void setTopK(float topK) {
		this.topK = topK;
	}
	public float getTopP() {
		return topP;
	}
	public void setTopP(float topP) {
		this.topP = topP;
	}
	public float getVersion() {
		return version;
	}
	public void setVersion(float version) {
		this.version = version;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}

}
