# Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.4.7/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.4.7/maven-plugin/reference/html/#build-image)
* [GCP Support](https://googlecloudplatform.github.io/spring-cloud-gcp/reference/html/index.html)

### Guides
The following guides illustrate how to use some features concretely:

* [GCP Samples](https://github.com/GoogleCloudPlatform/spring-cloud-gcp/tree/master/spring-cloud-gcp-samples)



### Service account creation

https://cloud.google.com/docs/authentication/production#auth-cloud-implicit-java


### Spring Datastore Doc

https://docs.spring.io/spring-cloud-gcp/docs/current/reference/html/datastore.html

[Spring Data for Google Datastore](https://googlecloudplatform.github.io/spring-cloud-gcp/reference/html/datastore.html#_relationships)


### Set Data store credentials by running

export GOOGLE_APPLICATION_CREDENTIALS=mbcc-quiz-4b4eb547b8b1.json

export GOOGLE_APPLICATION_CREDENTIALS=mbcc-emergency-a02d8d4ea7dd.json

### Run app by invoking
./mvnw spring-boot:run

### Deploy to GCP
gcloud config set project 'projectname'

./mvnw -DskipTests package appengine:deploy - this does not work


http://mbcc-quiz.appspot.com/quiz/0891d267-48a7-40ee-afc7-0e6c5f3c6404
http://mbcc-quiz.appspot.com/quizAll


http://localhost:4200/add/bcc9d663-dbf5-4642-aa9b-01f5cfbd2eb3


if getting deployment error
ensure you have the latest gcloud

`gcloud components update`

and then run

./mvnw -DskipTests package
gcloud app deploy --no-cache

the above command worked
